﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {

	Pile pile;
	PileColumn column;
	PileFreeCell freecell;
	PileFundation foundation;

	// Use this for initialization
	void Start () {
		//========Test case 1: create a full deck===========
//		pile = (Instantiate(GameManager.Instance.PilePrefab) as GameObject).GetComponent<Pile>();
//
//		pile.CreateCards (false);
//
//		pile.Shuffle ();
//		pile.TestCards ();


		//=========Test case 2: Test column legal/illegal logic========
//		column = (Instantiate (GameManager.Instance.PileColumnPrefab) as GameObject).GetComponent<PileColumn> ();
//
//		Card card1 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card1.suit = CardSuit.CLUBS;
//		card1.face = CardFace.THREE;
//		card1.color = 0;
//		Card card2 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card2.suit = CardSuit.DIAMONDS;
//		card2.face = CardFace.DEUCE;
//		card2.color = 1;
//		Card card3 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card3.suit = CardSuit.DIAMONDS;
//		card3.face = CardFace.FOUR;
//		card3.color = 1;



//		column.AddCard (card1);

//		Debug.Log ("column ");
//
////		column.TestCards ();
//		column.AddCard (card2);
//		Debug.Log ("column ");
//
//		column.TestCards ();
//		column.AddCard (card3);
//
//		Debug.Log ("column ");
//
//		column.TestCards ();

		//========Test case 3: Test Freecell logic========

//		freecell = (Instantiate (GameManager.Instance.PileFreeCellPrefab) as GameObject).GetComponent<PileFreeCell> ();
//
//
//		Card card4 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card4.suit = CardSuit.CLUBS;
//		card4.face = CardFace.ACE;
//		card4.color = 0;
//		Card card5 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card5.suit = CardSuit.CLUBS;
//		card5.face = CardFace.DEUCE;
//		card5.color = 0;
//		Card card6 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card6.suit = CardSuit.DIAMONDS;
//		card6.face = CardFace.FOUR;
//		card6.color = 1;
//		Card card7 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card7.suit = CardSuit.DIAMONDS;
//		card7.face = CardFace.FOUR;
//		card7.color = 1;
//		Card card8 = (Instantiate(GameManager.Instance.CardPrefab) as GameObject).GetComponent<Card>();
//		card8.suit = CardSuit.DIAMONDS;
//		card8.face = CardFace.FOUR;
//		card8.color = 1;
//
//		Debug.Log ("Freecell");
//		freecell.AddCard (card4);
//		Debug.Log ("Freecell");
//		freecell.AddCard (card5);
//		Debug.Log ("Freecell");
//		freecell.AddCard (card6);
//		Debug.Log ("Freecell");
//		freecell.AddCard (card7);
//		Debug.Log ("Freecell");
//		freecell.AddCard (card8);
//
//		freecell.TestCards ();

		//=======Test case 4: Test foundation logic=========
//		foundation = (Instantiate(GameManager.Instance.PileFoundationPrefab) as GameObject).GetComponent<PileFundation>();
//		Debug.Log ("foundation");
//		foundation.AddCard (card4);
//		Debug.Log ("foundation");
//		foundation.AddCard (card5);
//
//		Debug.Log ("foundation");
//		foundation.AddCard (card6);
//		foundation.TestCards ();


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
