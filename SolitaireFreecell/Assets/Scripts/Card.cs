﻿using UnityEngine;
using System.Collections;
using System;


public class Card : MonoBehaviour{
//	public Suit suit {get; set;}
//	public Face face {get; set;}

	public CardSuit suit {get; set;}
	public CardFace face {get; set;}

	public bool cardVisible;

	public int color; //red = 0; black = 1;
	public int columnNum;

	public bool isDraggable;

	public int sortingOrder;
	private int tarColNum;

	private Vector3 oriPos;
	private bool isDragging;
	public bool isValidMove;
	public bool isColliding;

	public Card (){
		//tba

	}

	public Card (CardSuit suit, CardFace face){
		this.suit = suit;
		this.face = face;

		if (suit == CardSuit.SPADES|| suit == CardSuit.CLUBS) {
			this.color = 1;
			Debug.Log ("black card spades or clubs");
		}
		if (suit == CardSuit.HEARTS || suit == CardSuit.DIAMONDS) {
			this.color = 0;
			Debug.Log ("red card hearts or diamonds");
		}
	}

	public void setColumnNum(int num){
		this.columnNum = num;
	}

	public int getColor(){
		return this.color;
	}

	public void Start(){
		isDraggable = false;
		isDragging = false;
		isValidMove = false;
		isColliding = false;

		String resourceName = (((int)this.face).ToString() + "_" + this.suit.ToString());
		if (cardVisible) {
			this.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> (resourceName);
			//this.GetComponent<Collider2D> ().enabled = true;
		} else {
			this.GetComponent<SpriteRenderer> ().sprite = null;
			//this.GetComponent<Collider2D> ().enabled = false;
		}

		sortingOrder = this.GetComponent<SpriteRenderer> ().sortingOrder;
	}

	public void Update(){

		if (isDraggable) {
			this.GetComponent<Collider2D> ().enabled = true;

		} else {
			this.GetComponent<Collider2D> ().enabled = false;

		}

	}


	void OnMouseDown(){
		Debug.Log (this.name);
		oriPos = transform.position;
		if (this.gameObject.GetComponent<Rigidbody2D> () == null) {
			this.gameObject.AddComponent<Rigidbody2D> ();
			this.GetComponent<Rigidbody2D> ().isKinematic = true;
		}
	}

	void OnMouseDrag(){
		Vector2 cursorPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);//getting cursor position
		this.transform.position = new Vector3(cursorPosition.x, cursorPosition.y, this.transform.position.z);
		this.GetComponent<SpriteRenderer> ().sortingOrder = 100;
		isDragging = true;

	}

	void OnMouseUp(){
		if (isDragging) {
			if (isColliding) {
				if (!isValidMove) {
					this.GetComponent<SpriteRenderer> ().sortingOrder = sortingOrder;
					this.transform.position = oriPos;
					if (this.gameObject.GetComponent<Rigidbody2D> () != null) {
						Rigidbody2D rigidbody = this.GetComponent<Rigidbody2D> ();
						Destroy (rigidbody);
					}
					isDragging = false;

				} else {
			
					if (this.gameObject.GetComponent<Rigidbody2D> () != null) {
						Rigidbody2D rigidbody = this.GetComponent<Rigidbody2D> ();
						Destroy (rigidbody);
					}
					String targetColumnName = "column" + tarColNum;
					PileColumn targetColumn = GameObject.Find (targetColumnName).GetComponent<PileColumn> ();
					targetColumn.AddCard (this);
					String thisColumnName = "column" + columnNum;
					PileColumn thisColumn = GameObject.Find (thisColumnName).GetComponent<PileColumn> ();
					thisColumn.RemoveCard ();
					Debug.Log ("new column");
					targetColumn.TestCards ();
					Debug.Log ("this column");
					thisColumn.TestCards ();
					this.transform.position = new Vector3 (targetColumn.transform.position.x, targetColumn.pileOfCards [targetColumn.pileOfCards.Count - 2].transform.position.y - 0.5f, targetColumn.transform.position.z);
					this.sortingOrder = targetColumn.pileOfCards [targetColumn.pileOfCards.Count - 2].sortingOrder + 1;
					isDragging = false;
				}
			} else {
				if (!isColliding) {
					for (int i = 0; i < 4; i++) {
						string freecellname = "freecell" + i; 
						PileFreeCell freecell = GameObject.Find (freecellname).GetComponent<PileFreeCell> ();
						Debug.Log (freecell.name + freecell.pileOfCards.Count);
						//
						if (freecell.ValidAddingCard (this) && this.face != CardFace.ACE) {
							freecell.AddCard (this);
							this.transform.position = freecell.transform.position;
							String thisColumnName = "column" + columnNum;
							PileColumn thisColumn = GameObject.Find (thisColumnName).GetComponent<PileColumn> ();
							thisColumn.RemoveCard ();
							isColliding = false;
							break;
						} else {
							this.transform.position = this.oriPos;
							isColliding = false;
						}
					}
				}
				//Debug.Log (isColliding);

			}
		}

	}

	void OnTriggerEnter2D(Collider2D coll) {
		//Debug.Log ("Collides");
		if (isDragging) {
			if (coll.gameObject.tag == "Card") {
				isColliding = true;
//			coll.gameObject.SendMessage("ApplyDamage", 10);
				int targetColumnNum = (coll.gameObject.GetComponent<Card> ()).columnNum;
				String targetColumnName = "column" + targetColumnNum;
				PileColumn targetColumn = GameObject.Find (targetColumnName).GetComponent<PileColumn> ();
				Debug.Log (this.face + " " + this.columnNum + " collides with " + coll.gameObject.GetComponent<Card> ().columnNum + " " + coll.gameObject.GetComponent<Card> ().face + " in " + targetColumn.name);
				isValidMove = targetColumn.ValidAddingCard (this);
				if (isValidMove) {
					tarColNum = targetColumnNum;
				}
			}
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		//Debug.Log ("Collides");

		isColliding = false;
				

	}
		
}
