﻿using UnityEngine;
using System.Collections;

public class GameModel : MonoBehaviour {


	Pile pile;
	PileColumn[] columns;
	PileFreeCell[] freecells;
	PileFundation[] foundations;

	// Use this for initialization
	void Start () {

		pile = (Instantiate(GameManager.Instance.PilePrefab) as GameObject).GetComponent<Pile>();
		
		pile.CreateCards (false);
		
		pile.Shuffle ();
		pile.TestCards ();

		columns = new PileColumn[8];
		freecells = new PileFreeCell[4];
		foundations = new PileFundation[4];

		Debug.Log (pile.pileOfCards[0].face);

		for (int i = 0; i < columns.Length; i++) {
			columns[i] =  (Instantiate(GameManager.Instance.PileColumnPrefab) as GameObject).GetComponent<PileColumn>();
			columns [i].name = "column" + i;
		}

		for(int j = 0; j < 4; j++){
			for (int i = 7 * j; i < 7 * (j+1); i++) {
				columns [j].ForceAddCard (pile.pileOfCards [i]);
			}
			columns [j].transform.position = new Vector3 (columns [j].transform.position.x - (4-j) * 2f, columns [j].transform.position.y, columns [j].transform.position.z);
		}

		for(int j = 4; j < 8; j++){
			for (int i = 28 + 6 * (j-4); i < 6 * (j-3) + 28; i++) {
				columns [j].ForceAddCard (pile.pileOfCards [i]);
			}
			columns [j].transform.position = new Vector3 (columns [j].transform.position.x + (j-4) * 2f, columns [j].transform.position.y, columns [j].transform.position.z);
		}

		
		for(int j = 0; j < 4; j++){
			for (int i = 0; i < columns[j].pileOfCards.Count; i ++) {
				Card card = columns [j].CreateCard (columns[j].pileOfCards[i].suit, columns[j].pileOfCards[i].face, columns[j].pileOfCards[i].color, true);
				card.setColumnNum (j);
				card.GetComponent<SpriteRenderer> ().sortingOrder = i;
				//columns [j].pileOfCards[i].transform.position = new Vector3 (columns [j].pileOfCards[i].transform.position.x - j * 2f, columns [j].pileOfCards[i].transform.position.y - 0.5f * i, columns [j].pileOfCards[i].transform.position.z);
				card.transform.position = new Vector3 (columns [j].transform.position.x, columns [j].pileOfCards[i].transform.position.y - 0.5f * i, columns [j].transform.position.z);
				card.name = j + "_" + card.suit + "_" + card.face;
				columns [j].pileOfCards [i] = card;
			}
		}

		for(int j = 4; j < 8; j++){
			for (int i = 0; i < columns[j].pileOfCards.Count; i ++) {
				Card card = columns [j].CreateCard (columns[j].pileOfCards[i].suit, columns[j].pileOfCards[i].face, columns[j].pileOfCards[i].color, true);
				card.setColumnNum (j);
				card.GetComponent<SpriteRenderer> ().sortingOrder = i;
				//columns [j].pileOfCards[i].transform.position = new Vector3 (columns [j].pileOfCards[i].transform.position.x - j * 2f, columns [j].pileOfCards[i].transform.position.y - 0.5f * i, columns [j].pileOfCards[i].transform.position.z);
				card.transform.position = new Vector3 (columns [j].transform.position.x, columns [j].pileOfCards[i].transform.position.y - 0.5f * i, columns [j].transform.position.z);
				card.name = j + "_" + card.suit + "_" + card.face;
				columns [j].pileOfCards [i] = card;
			}
		}

		for (int j = 0; j < 8; j++) {
			
			Debug.Log ("current column " + j);
			columns [j].TestCards ();
		}
//		Debug.Log (columns[0].pileOfCards.Count + " " + columns[1].pileOfCards.Count + " " + columns[2].pileOfCards.Count + " " + columns[3].pileOfCards.Count);


		for(int i = 0; i < freecells.Length; i++){
			freecells[i] =  (Instantiate(GameManager.Instance.PileFreeCellPrefab) as GameObject).GetComponent<PileFreeCell>();
			freecells [i].transform.position = new Vector3 (columns [i].transform.position.x, freecells [i].transform.position.y, freecells [i].transform.position.z);
			freecells [i].name = "freecell" + i;
		}

		for(int i = 0; i < foundations.Length; i++){
			foundations[i] =  (Instantiate(GameManager.Instance.PileFoundationPrefab) as GameObject).GetComponent<PileFundation>();
			foundations [i].transform.position = new Vector3 (columns [i + 4].transform.position.x, freecells [i].transform.position.y, freecells [i].transform.position.z);
			foundations [i].name = "foundation" + i;
		}

			

	}

	
	// Update is called once per frame
	void Update () {
		for (int j = 0; j < 8; j++) {
			for (int i = 0; i < columns [j].pileOfCards.Count - 2; i++) {
				columns [j].pileOfCards [i].isDraggable = false;
			}
		}

		for (int j = 0; j < 8; j++) {
			columns [j].pileOfCards [columns[j].pileOfCards.Count - 1].isDraggable = true;
		}

		GameObject cardclone = GameObject.Find ("Card(Clone)");
		if(cardclone != null)
			Destroy (cardclone);
	}


}
